<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 16.04.2019
 * Time: 13:48
 */
class Basket extends Father
{
    public function getTotalPrice()
    {
        $total_price = 0;
        foreach ($this->items as $item)
        {
            $total_price += $item['cost'] * $item['amount'];
        }
        return $total_price;
    }
    public function getItems()
    {
        $query = "SELECT basket.amount, products.name, products.cost, products.id, basket.id basket_id 
                  FROM basket
                  CROSS JOIN products 
                  ON basket.product_id=products.id 
                  WHERE basket.user_id=".USER_ID."
                  ";
        $items = $this->selectArray($query);
        if ($items)
        {
            $this->items = $items;
        }
        return null;
    }

    public function addItem($product_id, $amount = 1)
    {
        $amount = (float) $amount;
        if ($amount - intval($amount) == 0 && $amount > 0) {
            $query = "SELECT amount, id
                      FROM basket
                      WHERE product_id=$product_id
                      AND user_id=" . USER_ID . "
                      LIMIT 1;
            ";

            $result = mysqli_query($this->link, $query);
            $item = $this->selectArray($query)[0];

            if (!$item) {
                $query = "INSERT INTO basket (product_id, user_id, amount) VALUES($product_id, " . USER_ID . ", $amount )";
                $result = mysqli_query($this->link, $query);
            } else {
                $this->updateItemAmount($item['id'], $item['amount'] + 1);
            }
        }



        return $result;

    }
    public function updateItemAmount($item_id, $amount )
    {
        $amount = (float) $amount;
        if ($amount - intval($amount) == 0 && $amount > 0){
            $query = "UPDATE basket SET amount=$amount WHERE id=$item_id LIMIT 1";
            $result = mysqli_query($this->link, $query);
            return $result;
        }
    }
    public function deleteItem($item_id)
    {
        $query = "DELETE FROM basket WHERE id=$item_id LIMIT 1";
        $result = mysqli_query($this->link, $query);
        return $result;
    }
}