<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 16.04.2019
 * Time: 15:15
 */
class Catalog extends Father
{
    public function getItems()
    {
        $query = "
            SELECT products.name, products.cost, products.id 
            FROM products
        ";

        $result = $this->selectArray($query);
        if ($result)
        {
            $this->items = $result;
        }

        return 0;
    }
}