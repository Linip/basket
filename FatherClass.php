<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 16.04.2019
 * Time: 13:47
 */
class Father
{
    function __construct()
    {
        $this->connectDatabase();

    }

    protected $link;

    public $items = array();

    protected function connectDatabase()
    {
        $this->link = mysqli_connect(HOST, USER, PASSWORD,DATABASE);
    }

    protected function selectArray($query)
    {
        $result = mysqli_query($this->link, $query);
        $i = 0;
        if (mysqli_num_rows($result))
        while ($elem = mysqli_fetch_assoc($result))
        {
            $items[$i] = $elem;
            $i++;
        }
        else return 0;
        return $items;
    }

    protected function insert($query)
    {

    }

    function __destruct()
    {
        mysqli_close($this->link);
    }
}