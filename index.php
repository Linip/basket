<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 15.04.2019
 * Time: 9:36
 */

define('HOST', 'basket');
define('DATABASE', 'basket');
define('USER', 'mysql');
define('PASSWORD', 'mysql');
define('USER_ID', 2);


include_once('FatherClass.php');
include_once('BasketClass.php');
include_once('CatalogClass.php');



$basket = new Basket;
$catalog = new Catalog;

if (isset($_POST)) {
    $post = $_POST;
    if (isset($post['deleteItem'])) {
        $basket->deleteItem($post['deleteItem']);
    }
    if (isset($post['addItem'])) {
        $basket->addItem($post['addItem']);
    }
    if (isset($post['updateItem'])) {
        $basket->updateItemAmount($post['updateItem'], $post['amount']);
    }
}

$basket->getItems(USER_ID);
$catalog->getItems();


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Basket</title>
        <style>

        </style>

    </head>
    <body>
    <div>
        <h1>Корзина</h1>
        <table>
            <tr>
                <th>ID товара</th>
                <th>Имя</th>
                <th>Цена</th>
                <th>Колл-во</th>
                <th></th>
            </tr>
            <?foreach($basket->items as $item):?>
                <tr>
                    <td><?= $item['id'];?></td>
                    <td><?= $item['name'];?></td>
                    <td><?= $item['cost'];?></td>
                    <td><?= $item['amount'];?></td>
                    <td>
                        <form action="/" method="post">
                            <input style="width: 30px" type="text" name="amount">
                            <button type="submit" value="<?= $item['basket_id'];?>" name="updateItem">Изменить колличество</button>
                        </form>
                    </td>
                    <td>
                        <form action="/" method="post">
                            <button type="submit" value="<?= $item['basket_id'];?>" name="deleteItem">Удалить</button>
                        </form>
                    </td>
                </tr>
            <?endforeach;?>
        </table>
        <h4>ИТОГО: <?= $basket->getTotalPrice()?></h4>

    </div>
    <div>
        <h1>Товары</h1>
        <table>
            <tr>
                <th>ID товара</th>
                <th>Имя</th>
                <th>Цена</th>
                <th></th>
            </tr>
            <?foreach($catalog->items as $item):?>
                <tr>
                    <td><?= $item['id'];?></td>
                    <td><?= $item['name'];?></td>
                    <td><?= $item['cost'];?></td>
                    <td>
                        <form action="/" method="post">
                            <button type="submit" value="<?= $item['id'];?>" name="addItem">Добавить в корзину</button>
                        </form>
                    </td>
                </tr>
            <?endforeach;?>
        </table>
    </div>

    </body>
</html>